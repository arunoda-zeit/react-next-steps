import * as React from "react";
import axios from "axios";
import FactCont from "./FactCont";

interface IFactCategoryContProps {
  titel: string;
}
interface IFactCategoryContState {
  kategorien: Array<string>;
  fakt: string;
}

interface IPersonData {
  id: string;
  name: string;
  firstname: string;
  birthday: Date;
  move: string;
}

class FactCategoryCont extends React.Component<
  IFactCategoryContProps,
  IFactCategoryContState
> {
  readonly api: string = "https://api.chucknorris.io/jokes";
  readonly postApi: string = "https://jsonplaceholder.typicode.com/posts";

  readonly data: IPersonData = {
    id: '1000',
    name: 'Norris',
    firstname: 'Chuck',
    move: 'Roundhouse Kick',
    birthday: new Date()
  };

  categoryChangedHandler = (kategorie: string) => {
    console.log("Chosen:", kategorie);
    axios
    .get(this.api + "/random?category=" + kategorie)
    .then(response => {
        console.log(response.data.value);
        this.setState({
            fakt: response.data.value
        })
    })
    .catch(error => {
      console.log(error);
    });
  };

  constructor(props: IFactCategoryContProps, state: IFactCategoryContState) {
    super(props, state);
    this.state = {
      kategorien: [],
      fakt: ""
    };
  }

  postSomeDataHandler = () => {
    const url: string = this.postApi;
    console.log("Post URL", url);
    console.log("Post Request", this.data);
    axios.post(url, this.data).then(response => {
      console.log("Post Reponse", response);
    }).catch(error => {
      console.log("Post error", error);
    })
  }

  deleteSomeDataHandler = () => {
    const url: string = this.postApi + "/" + this.data.id;
    console.log("Delete URL", url);
    axios.delete(url)
    .then(response => {
      console.log("Delete Reponse", response);
    })
    .catch(error => {
      console.log("Delete error", error);
    });
  }

  updateSomeDataHandler = () => {
    const url: string = this.postApi + "/" + this.data.id;
    console.log("Update URL", url);
    axios.put(url, this.data)
    .then(response => {
      console.log("Update Reponse", response);
    })
    .catch(error => {
      console.log("Update error", error);
    })
  }

  public componentDidMount() {
    axios
      .get<Array<string>>(this.api + "/categories")
      .then(response => {
        const kategorien: Array<string> = [];
        console.log(response);
        response.data.map(kategorie => {
          kategorien.push(kategorie);
        });
        this.setState({
          kategorien: kategorien
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  public render() {
    let choices = (
      <div className="auswahl">
        {this.state.kategorien.map(kategorie => {
          return (
            <div key={kategorie}>
              <input
                type="radio"
                name="kategorie"
                onChange={this.categoryChangedHandler.bind(this, kategorie)}
              />
              {kategorie}
            </div>
          );
        })}
      </div>
    );

    return (
      <div>
        <h3>{this.props.titel}</h3>
        {choices}
        <FactCont fakt={this.state.fakt} />
        <button onClick={this.postSomeDataHandler} className="nicebutton">Post some data</button>
        <button onClick={this.deleteSomeDataHandler} className="nicebutton">Delete some data</button>
        <button onClick={this.updateSomeDataHandler} className="nicebutton">Update some data</button>
      </div>
    );
  }
}

export default FactCategoryCont;